mypy==1.2.0
mypy-extensions==1.0.0
pip==22.3.1
PyYAML==6.0
setuptools==65.6.3
tomli==2.0.1
typing_extensions==4.5.0
wheel==0.38.4
