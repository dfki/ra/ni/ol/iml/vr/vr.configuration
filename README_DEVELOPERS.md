# VR.Hercules (For Developers)

In addition to the main [README.md](README.md),
    this file documents how to set up, develop and, maintain this project.

## Cloning the Repository

The source code of this repository is hosted on our GitLab.
To create a local clone:

```bash
git clone git@gitlab.com:dfki/ra/dfki-ni/ol/dfki-iml/vr/vr.hercules.git VR.Hercules
cd VR.Hercules
```

## Virtual Environment

The setup time requirements should be installed into a virtual environment:

```bash
python3.10 -m venv venv
venv/bin/python -m pip install --upgrade pip setuptools wheel
venv/bin/python -m pip install --upgrade -r requirements.d/tox.in
venv/bin/python -m pip freeze --all > requirements.d/tox.txt
```

## Test Environment

This project contains automation for setting up a test time environment:

```bash
venv/bin/python -m tox -e py310 --notest
```

## Running Tests

Tox can run the tests in the test environment:

```bash
venv/bin/python -m tox -e py310
```

## Upgrading Requirements

-   Use a tool of your choice.

## Building the Documentation

```bash
venv/bin/python -m tox -e sphinx
```

Afterwards, open [public/index.html](public/index.html).
